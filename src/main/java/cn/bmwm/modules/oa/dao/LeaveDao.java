/**
 * There are <a href="https://www.bmwm.cn">bmwm.cn</a> code generation
 */
package cn.bmwm.modules.oa.dao;

import org.springframework.stereotype.Repository;

import cn.bmwm.common.persistence.BaseDao;
import cn.bmwm.common.persistence.Parameter;
import cn.bmwm.modules.oa.entity.Leave;

/**
 * 请假DAO接口
 * @author www.bmwm.cn
 * @version 2013-8-23
 */
@Repository
public class LeaveDao extends BaseDao<Leave> {
	
	public int updateProcessInstanceId(String id,String processInstanceId){
		return update("update Leave set processInstanceId=:p1 where id = :p2", new Parameter(processInstanceId, id));
	}
	
}
