/**
* Copyright &copy; 2014-2016 <a href="https://www.bmwm.cn">bmwm.cn</a> All rights reserved.
 */
package cn.bmwm.modules.sys.dao;

import org.springframework.stereotype.Repository;

import cn.bmwm.common.persistence.BaseDao;
import cn.bmwm.modules.sys.entity.Log;

/**
 * 日志DAO接口
 * @author www.bmwm.cn
 * @version 2013-8-23
 */
@Repository
public class LogDao extends BaseDao<Log> {

}
