/**
* Copyright &copy; 2014-2016 <a href="https://www.bmwm.cn">bmwm.cn</a> All rights reserved.
 */
package cn.bmwm.modules.sys.dao;

import java.util.List;

import cn.bmwm.common.persistence.annotation.MyBatisDao;
import cn.bmwm.modules.sys.entity.Dict;

/**
 * MyBatis字典DAO接口
 * @author www.bmwm.cn
 * @version 2013-8-23
 */
@MyBatisDao
public interface MyBatisDictDao {
	
    Dict get(String id);
    
    List<Dict> find(Dict dict);
    
}
