/**
* Copyright &copy; 2014-2016 <a href="https://www.bmwm.cn">bmwm.cn</a> All rights reserved.
 */
package cn.bmwm.modules.sys.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.bmwm.common.persistence.BaseDao;
import cn.bmwm.common.persistence.Parameter;
import cn.bmwm.modules.sys.entity.Office;

/**
 * 机构DAO接口
 * @author www.bmwm.cn
 * @version 2013-8-23
 */
@Repository
public class OfficeDao extends BaseDao<Office> {
	
	public List<Office> findByParentIdsLike(String parentIds){
		return find("from Office where parentIds like :p1", new Parameter(parentIds));
	}
	
//	@Query("from Office where (id=?1 or parent.id=?1 or parentIds like ?2) and delFlag='" + Office.DEL_FLAG_NORMAL + "' order by code")
//	public List<Office> findAllChild(Long parentId, String likeParentIds);
	
}
