/**
* Copyright &copy; 2014-2016 <a href="https://www.bmwm.cn">bmwm.cn</a> All rights reserved.
 */
package cn.bmwm.modules.sys.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.bmwm.common.persistence.BaseDao;
import cn.bmwm.common.persistence.Parameter;
import cn.bmwm.modules.sys.entity.Dict;

/**
 * 字典DAO接口
 * @author www.bmwm.cn
 * @version 2013-8-23
 */
@Repository
public class DictDao extends BaseDao<Dict> {

	public List<Dict> findAllList(){
		return find("from Dict where delFlag=:p1 order by sort", new Parameter(Dict.DEL_FLAG_NORMAL));
	}

	public List<String> findTypeList(){
		return find("select type from Dict where delFlag=:p1 group by type", new Parameter(Dict.DEL_FLAG_NORMAL));
	}
}
