/**
* Copyright &copy; 2014-2016 <a href="https://www.bmwm.cn">bmwm.cn</a> All rights reserved.
 */
package cn.bmwm.modules.sys.security;

import org.apache.shiro.authc.AuthenticationException;

/**
 * 验证码异常处理类
 * @author www.bmwm.cn
 * @version 2013-5-19
 */
public class CaptchaException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public CaptchaException() {
		super();
	}

	public CaptchaException(String message, Throwable cause) {
		super(message, cause);
	}

	public CaptchaException(String message) {
		super(message);
	}

	public CaptchaException(Throwable cause) {
		super(cause);
	}

}
