/**
 * There are <a href="https://www.bmwm.cn">bmwm.cn</a> code generation
 */
package cn.bmwm.modules.prj.dao;

import org.springframework.stereotype.Repository;

import cn.bmwm.common.persistence.BaseDao;
import cn.bmwm.modules.prj.entity.Project;

/**
 * 项目DAO接口
 * @author www.bmwm.cn
 * @version 2013-12-07
 */
@Repository
public class ProjectDao extends BaseDao<Project> {
	
}
